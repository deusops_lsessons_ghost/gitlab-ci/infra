%{ for key, values in vms ~}
[${key}]
%{ if key == "dev" ~}
${values.hostname} ansible_host=${ip_dev} ansible_user=${values.user_name} ansible_ssh_private_key_file=${values.ssh_key_path}
%{ else ~}
${values.hostname} ansible_host=${ip_prod} ansible_user=${values.user_name} ansible_ssh_private_key_file=${values.ssh_key_path}
%{ endif }
%{ endfor ~}
[all:children]
%{ for key, values in vms ~}
${key}
%{ endfor }
[all:vars]
d_name = ${domain_name}
