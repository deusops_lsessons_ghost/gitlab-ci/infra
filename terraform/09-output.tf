output "external_ips" {
  value = {
    for instance in yandex_compute_instance.vm:
    instance.name => yandex_compute_instance.vm[instance.name].network_interface.0.nat_ip_address
  }
}

resource "local_file" "ansible_hosts" {
  content = templatefile("./template/hosts.tpl", {
    domain_name = var.domain-name
    vms         = local.virtual_machines
    ip_dev      = yandex_compute_instance.vm["dev"].network_interface.0.nat_ip_address
    ip_prod     = yandex_compute_instance.vm["prod"].network_interface.0.nat_ip_address
  })
filename = "../ansible/hosts"
}
