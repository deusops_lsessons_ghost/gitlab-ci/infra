resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "DeusOps class public zone"

  labels = {
    label1 = "deusopsclass.domain"
  }

  zone   = "${var.domain-name}."
  public = true
}

resource "yandex_dns_recordset" "prod" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "${var.domain-name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm["prod"].network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "traefik_prod" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "traefik"
  type    = "CNAME"
  ttl     = 200
  data    = [yandex_dns_recordset.prod.name]
}

resource "yandex_dns_recordset" "dev" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "${var.domain-name-prefix}.${var.domain-name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm["dev"].network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "wildcard_dev" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "*.${var.domain-name-prefix}"
  type    = "CNAME"
  ttl     = 200
  data    = [yandex_dns_recordset.dev.name]
}
