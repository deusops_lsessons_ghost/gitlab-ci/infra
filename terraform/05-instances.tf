locals {
  virtual_machines = {
    "dev" = {
      hostname     = "develop"
      memory       = var.dev-memory
      user_name    = var.dev-user
      ssh_key_path = var.dev-ssh-key-path
      external_ip  = true
      }
    "prod" = {
      hostname     = "production"
      user_name    = var.prod-user
      ssh_key_path = var.prod-ssh-key-path
      external_ip  = true
      }
  }
}

data "template_file" "user_config" {
  for_each    = local.virtual_machines
  template    = file(var.instance-user-template)
  vars = {
    ssh_user  = each.value.user_name
    ssh_key   = file(join("", [each.value.ssh_key_path, ".pub"]))
  }
}

resource "yandex_compute_instance" "vm" {
  for_each        = local.virtual_machines
  name            = each.key
  hostname        = each.value.hostname
  description     = "${each.value.hostname} instance"

  platform_id               = var.instance-platform-id
  allow_stopping_for_update = true

  resources {
    cores         = var.instance-cores
    memory        = try(each.value.memory, var.instance-memory)
    core_fraction = var.instance-core-fraction
  }

  boot_disk {
    initialize_params {
      image_id    = var.instance-image-id
      type        = var.instance-disk-type
      size        = try(each.value.disk-size, var.instance-disk-size)
    }
  }

  network_interface {
    subnet_id     = yandex_vpc_subnet.subnet_terraform.id
    nat           = try(each.value.external_ip, false)
  }

  scheduling_policy {
    preemptible   = var.is-auto-shutdown
  }

  metadata = {
    user-data = data.template_file.user_config[each.key].rendered
  }
}

resource "null_resource" "ansible" {
  depends_on = [
    yandex_compute_instance.vm,
    local_file.ansible_hosts
  ]

  # Play ansible playbook
  provisioner "local-exec" {
    on_failure    = continue
    command       = "sleep 30 && ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -T 20 -i ../ansible/hosts ../ansible/playbook.yml"
  }

  # Unregister all gitlab-runners
  provisioner "local-exec" {
    on_failure    = continue
    when          = destroy
    command       = "ansible all -i ../ansible/hosts -b -m command -a 'gitlab-runner unregister --all-runners'"
  }
}
